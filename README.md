# vim-ale-example-vanilla-js

A tiny example of a JS app with the formatter [Prettier][prettier] and
linter [ESlint][eslint] in Vim using the Vim plugin [ALE][ale].

### Screencast

![screencast-gif][screencast-gif]

### Steps

Tested on
* Prettier v.2.7.1
* ESLint v.8.26.0
* Vim v.9.0
* ALE v.3.20 

```bash
git init vim-ale-example-vanilla-js
cd vim-ale-example-vanilla-js
npm init -y
npm i -D eslint eslint-plugin-prettier prettier
echo -e "build\ncoverage" > .prettierignore
echo '{
  "semi": false,
  "singleQuote": true
}' > .prettierrc.json
echo '{
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": "error"
  }
}' > .eslintrc.json
```

### Vim config

Make sure your .vimrc contains:

```vimscript
let g:ale_linters = {
\   'javascript': ['eslint'],
\}
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\}
nmap <silent> <leader>f <Plug>(ale_fix)
```

Remember to have your Node version available in the shell that you use
for Vim.

[ale]: https://github.com/dense-analysis/ale
[eslint]: https://eslint.org/
[prettier]: https://github.com/sheerun/prettier-standard
[screencast-gif]: https://i.imgur.com/9TsLfAy.gif
